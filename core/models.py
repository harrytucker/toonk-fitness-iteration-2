from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
import django.utils.timezone


class Food(models.Model):
    """ food model - stores information about an item of food """
    food_name = models.TextField(unique=True)
    calories_per_100g = models.FloatField()

    def __str__(self):
        return "Name: %s Calories (100g): %s" % (self.food_name, self.calories_per_100g)


class Drink(models.Model):
    """ drink model - stores information about an item of drink """
    drink_name = models.TextField(unique=True)
    calories_per_100ml = models.FloatField()

    def __str__(self):
        return "Name: %s Calories (100g): %s" % (self.drink_name, self.calories_per_100ml)


class ExerciseType(models.Model):
    """ exercise type - differentiates between types of exercise """
    exercise_type_name = models.TextField(unique=True)
    has_distance = models.BooleanField()
    has_duration = models.BooleanField()

    def __str__(self):
        return "Name: %s Has Distance?: %s Has Duration: %s" % (self.exercise_type_name,
                                                                self.has_distance,
                                                                self.has_duration)


class Exercise(models.Model):
    """ exercise model - stores information about a bout of exercise """
    exercise_name = models.TextField(unique=True)
    date_time_added = models.DateTimeField()
    type_of_exercise = models.ForeignKey(ExerciseType, on_delete=models.PROTECT)
    duration = models.DurationField()
    distance = models.FloatField()

    def __str__(self):
        return "Name: %s Date: %s Type: %s Duration: %s Distance %s" % (self.exercise_name,
                                                                        self.date_time_added,
                                                                        self.type_of_exercise,
                                                                        self.duration,
                                                                        self.distance)


class WeightGoal(models.Model):
    """ weight goal - describes a weight goal """
    target_weight = models.FloatField()
    need_to_gain_weight = models.BooleanField()

    def __str__(self):
        return "Target Weight: %s Gaining Weight?: %s" % (self.target_weight, self.need_to_gain_weight)


class ExerciseGoal(models.Model):
    """ exercise goal model - links to an exercise """
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)

    def __str__(self):
        return "Exercise: %s" % self.exercise


class Goal(models.Model):
    """ goal model - stores information about a goal """
    goal_name = models.TextField(unique=True)
    start_date = models.DateTimeField()
    target_date = models.DateTimeField()
    is_finished = models.BooleanField()
    is_met = models.BooleanField()
    weight_goal = models.ForeignKey(WeightGoal, on_delete=models.PROTECT, blank=True, null=True)
    exercise_goal = models.ForeignKey(ExerciseGoal, on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return "Name: %s Start: %s Target: %s Finished?: %s Met?: %s Weight Goal?: %s Exercise Goal?: %s" % (self.goal_name,
                                                                                                             self.start_date,
                                                                                                             self.target_date,
                                                                                                             self.is_finished,
                                                                                                             self.is_met,
                                                                                                             self.weight_goal,
                                                                                                             self.exercise_goal)


class Meal(models.Model):
    """ meal model - stores food and drink information to create a meal """
    BREAKFAST = 'BRE'
    LUNCH = 'LUN'
    DINNER = 'DIN'
    SNACK = 'SNA'

    MEAL_OF_THE_DAY = (
        (BREAKFAST, 'Breakfast'),
        (LUNCH, 'Lunch'),
        (DINNER, 'Dinner'),
        (SNACK, 'Snack'),
    )

    meal_name = models.TextField(unique=True)
    date_time_added = models.DateTimeField(default=django.utils.timezone.now)
    meal_of_the_day = models.CharField(max_length=3, choices=MEAL_OF_THE_DAY)
    food = models.ManyToManyField(Food, blank=True)
    food_amount_grams = models.FloatField(null=True)
    drink = models.ManyToManyField(Drink, blank=True)
    drink_amount_mls = models.FloatField(blank=True, null=True)
    calories = models.FloatField(default=0)

    def __str__(self):
        return "Name: %s Date: %s MoTD: %s Food: %s Food Amount: %s Drink: %s Drink Amount: %s Calories:%s" % (self.meal_name,
                                                                                                               self.date_time_added,
                                                                                                               self.meal_of_the_day,
                                                                                                               self.food,
                                                                                                               self.food_amount_grams,
                                                                                                               self.drink,
                                                                                                               self.drink_amount_mls,
                                                                                                               self.calories)


class Group(models.Model):
    """ user group - a group of users with shared goals """
    group_name = models.CharField(max_length=20, unique=True)
    ongoing_goals = models.ManyToManyField(Goal, related_name="ongoing_goals_relate", blank=True)
    completed_goals = models.ManyToManyField(Goal, related_name="completed_goals_relate", blank=True)

    def __str__(self):
        return "Name: %s Users: %s Ongoing Goals: %s Completed Goals: %s" % (self.group_name,
                                                                             self.users,
                                                                             self.ongoing_goals,
                                                                             self.completed_goals)


class Profile(models.Model):
    """ profile - extra information regarding user that doesn't involve authentication """
    MALE = 'M'
    FEMALE = 'F'

    GENDER = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    weight_kg = models.FloatField()
    height_m = models.FloatField()
    age = models.IntegerField()
    bmi = models.FloatField(null=True)
    gender = models.CharField(max_length=1, choices=GENDER)
    foods = models.ManyToManyField(Food, blank=True)
    drinks = models.ManyToManyField(Drink, blank=True)
    goals = models.ManyToManyField(Goal, blank=True)
    groups = models.ManyToManyField(Group, blank=True)
    exercises = models.ManyToManyField(Exercise, blank=True)
    meals = models.ManyToManyField(Meal, blank=True)

    def __str__(self):
        return "Name: %s %s" % (self.user.first_name, self.user.last_name)


# link django's user auth to creating the user's profile
# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Profile.objects.create(user=instance)
#
#
# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
#     instance.profile.save()
